-- tabel Product --
create table product(
                        id    varchar(40) PRIMARY KEY,
                        code  varchar(10)    NOT NULL UNIQUE,
                        name  varchar(255)   NOT NULL,
                        price decimal(19, 2) NOT NULL
) Engine=InnoDB;

