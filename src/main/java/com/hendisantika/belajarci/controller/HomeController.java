package com.hendisantika.belajarci.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * Project : belajar-ci
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 22/10/17
 * Time: 07.48
 * To change this template use File | Settings | File Templates.
 */

@RestController
public class HomeController {

    @GetMapping("/")
    String index(){
        return "Belajar CI  Heroku - Waktu Saat ini : " + new Date();
    }
}
