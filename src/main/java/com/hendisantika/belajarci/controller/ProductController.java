package com.hendisantika.belajarci.controller;

import com.hendisantika.belajarci.domain.Product;
import com.hendisantika.belajarci.exception.DataNotFoundException;
import com.hendisantika.belajarci.repository.ProductDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

/**
 * Created by IntelliJ IDEA.
 * Project : belajar-ci
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 21/10/17
 * Time: 20.56
 * To change this template use File | Settings | File Templates.
 */

@RestController
@RequestMapping("/api/products")
@Transactional(readOnly = true)
public class ProductController {
    @Autowired
    private ProductDao productDao;

    @PostMapping
    @Transactional(readOnly = false)
    public ResponseEntity<Void> create(@RequestBody @Valid Product p, UriComponentsBuilder uriBuilder) {
        productDao.save(p);
        URI location = uriBuilder.path("/api/products/{id}")
                .buildAndExpand(p.getId()).toUri();
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(location);
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @GetMapping
    public Page<Product> findAll(Pageable page) {
        return productDao.findAll(page);
    }

    @GetMapping("/{id}")
    public Product findById(@PathVariable("id") Product p) {
        if (p == null) {
            throw new DataNotFoundException("No data with the specified id");
        }

        return p;
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Transactional(readOnly = false)
    public void update(@PathVariable("id") String id, @RequestBody @Valid Product p) {
        if (!productDao.existsById(id)) {
            throw new DataNotFoundException("No data with the specified id");
        }
        p.setId(id);
        productDao.save(p);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    @Transactional(readOnly = false)
    public void delete(@PathVariable("id") String id) {
        if (!productDao.existsById(id)) {
            throw new DataNotFoundException("No data with the specified id");
        }
        productDao.deleteById(id);
    }








}
