package com.hendisantika.belajarci.repository;

import com.hendisantika.belajarci.domain.Product;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : belajar-ci
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 21/10/17
 * Time: 18.56
 * To change this template use File | Settings | File Templates.
 */
public interface ProductDao extends PagingAndSortingRepository<Product, String>{
}
