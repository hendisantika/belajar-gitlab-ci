-- insert tabel Product --
INSERT INTO product(id, code, name, price)
VALUES ('abc121', 'P-001', 'Product 001', 101000.01),
       ('abc122', 'P-002', 'Product 002', 101000.01),
       ('abc123', 'P-003', 'Product 003', 101000.01),
       ('abc124', 'P-004', 'Product 004', 101000.01),
       ('abc125', 'P-005', 'Product 005', 101000.01),
       ('abc126', 'P-006', 'Product 006', 101000.01),
       ('abc127', 'P-007', 'Product 007', 101000.01),
       ('abc128', 'P-008', 'Product 008', 2500.00),
       ('abc129', 'P-009', 'Product 009', 2500.00),
       ('abc130', 'P-030', 'Product 030', 2500.00);
